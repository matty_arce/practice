package Practice.App;

import unidad4.listas.Nodo;

public class Nodo<T> {
    T dato;
    Nodo<T> sigte;

    public Nodo() {
    }

    public Nodo(T dato) {
        this.dato = dato;
    }

    @Override
    public String toString() {
        return "Dato: " + dato + "\n";
    }

}