package Practice.Persona;

public class Persona {

	private int edad;
	private int cedula;
	private String nombre;
	private String status;
	
	public Persona(){
		
	}
	
	public Persona (String nombre,int cedula,int edad,String status){
		this.nombre = nombre;
		this.cedula = cedula;
		this.edad = edad;
		this.status = status;
	}
	
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getCedula() {
		return cedula;
	}
	public void setCedula(int cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Persona [edad=" + edad + ", cedula=" + cedula + ", nombre=" + nombre + "]";
	}
	
}
