package Practice.Excepciones;

public class ListaVaciaException extends Exception {

	public ListaVaciaException() {
		super("Lista esta vacia");
		
	}

	public ListaVaciaException(String message) {
		super(message);
		
	}

}
