package Practice.Excepciones;

public class ListaLlenaException extends Exception {

	public ListaLlenaException() {
		super("La lista esta llena");
		
	}

	public ListaLlenaException(String message) {
		super(message);

	}

}
