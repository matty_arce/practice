package Practice.Excepciones;

public class PosicionFueraRangoException extends Exception {

	public PosicionFueraRangoException() {
		super("Posicion esta fuera de rango");
		
	}

	public PosicionFueraRangoException(String message) {
		super(message);
		
	}

	
	
}
